#include "camera.h"
#include "dcp.h"
#include "gl_util.h"
#include <assert.h>
#include <stdio.h>

#define SHADER_DEBAYER 1
#define SHADER_DEBAYER_PACKED 2
#define SHADER_YUV 3

typedef struct {
        int format;
        int shader;
        float forward_matrix[9];

        GLuint frame_buffer;
        GLuint program;
        GLint uniform_transform;
        GLint uniform_pixel_size;
        GLint uniform_padding_ratio;
        GLint uniform_texture;
        GLint uniform_color_matrix;
        GLint uniform_row_length;
        GLint uniform_inv_gamma;
        GLint uniform_blacklevel;

        GLuint quad;
} GLES2Debayer;

GLES2Debayer *gles2_debayer_new(int format);

void gles2_debayer_free(GLES2Debayer *self);

void gles2_debayer_use(GLES2Debayer *self);

void gles2_debayer_configure(GLES2Debayer *self,
                             uint32_t dst_width,
                             uint32_t dst_height,
                             uint32_t src_width,
                             uint32_t src_height,
                             uint32_t rotation,
                             bool mirrored,
                             struct MPCameraCalibration calibration);

void gles2_debayer_set_shading(GLES2Debayer *self,
                               float red,
                               float blue,
                               float blacklevel);

void gles2_debayer_process(GLES2Debayer *self, GLuint dst_id, GLuint source_id);

const char* gles2_shader_to_fragment(int shader);
const char* gles2_shader_to_vertex(int shader);
