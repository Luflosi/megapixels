// This file should work with 10bit raw bayer packed formats. It is unlikely to work with 12bit or 14bit formats without a few changes.

#ifdef GL_ES
precision highp float;
#endif

uniform sampler2D texture;
uniform mat3 color_matrix;
uniform float inv_gamma;
uniform float blacklevel;
uniform float row_length;
uniform float padding_ratio;

varying vec2 top_left_uv;
varying vec2 top_right_uv;
varying vec2 bottom_left_uv;
varying vec2 bottom_right_uv;

vec2
skip_5th_pixel(vec2 uv)
{
    vec2 new_uv = uv;

    new_uv.x *= 0.8;
    new_uv.x += floor(uv.x * row_length / 5.0) / row_length;

    // Crop out padding
    new_uv.x *= padding_ratio;

    return new_uv;
}

void
main()
{
    // Note the coordinates for texture samples need to be a varying, as the
    // Mali-400 has this as a fast path allowing 32-bit floats. Otherwise
    // they end up as 16-bit floats and that's not accurate enough.
    vec4 samples = vec4(texture2D(texture, skip_5th_pixel(top_left_uv)).r,
    texture2D(texture, skip_5th_pixel(top_right_uv)).r,
    texture2D(texture, skip_5th_pixel(bottom_left_uv)).r,
    texture2D(texture, skip_5th_pixel(bottom_right_uv)).r);

    #if defined(CFA_BGGR)
    vec3 color = vec3(samples.w, (samples.y + samples.z) / 2.0, samples.x);
    #elif defined(CFA_GBRG)
    vec3 color = vec3(samples.z, (samples.x + samples.w) / 2.0, samples.y);
    #elif defined(CFA_GRBG)
    vec3 color = vec3(samples.y, (samples.x + samples.w) / 2.0, samples.z);
    #else
    vec3 color = vec3(samples.x, (samples.y + samples.z) / 2.0, samples.w);
    #endif

    color -= blacklevel;
    color *= color_matrix;

    vec3 gamma_color = pow(color, vec3(inv_gamma));
    gl_FragColor = vec4(gamma_color, 1);
}
