cmake_minimum_required(VERSION 3.14)
project(Megapixels C)

set(CMAKE_C_STANDARD 11)

# Use the package PkgConfig to detect GTK+ headers/library files
FIND_PACKAGE(PkgConfig REQUIRED)
PKG_CHECK_MODULES(GTK4 REQUIRED gtk4)
PKG_CHECK_MODULES(FEEDBACK REQUIRED libfeedback-0.0)
PKG_CHECK_MODULES(TIFF REQUIRED libtiff-4)
PKG_CHECK_MODULES(ZBAR REQUIRED zbar)
PKG_CHECK_MODULES(EPOXY REQUIRED epoxy)
PKG_CHECK_MODULES(MEGAPIXELS REQUIRED libmegapixels)
PKG_CHECK_MODULES(DNG REQUIRED libdng)
PKG_CHECK_MODULES(XLIB REQUIRED x11)
PKG_CHECK_MODULES(XRANDR REQUIRED xrandr)
PKG_CHECK_MODULES(WAYLAND REQUIRED wayland-client)
PKG_CHECK_MODULES(JPEG REQUIRED libjpeg)

# Setup CMake to use GTK+, tell the compiler where to look for headers
# and to the linker where to look for libraries
INCLUDE_DIRECTORIES(
        ${GTK4_INCLUDE_DIRS}
        ${FEEDBACK_INCLUDE_DIRS}
        ${DNG_INCLUDE_DIRS}
        ${ZBAR_INCLUDE_DIRS}
        ${EPOXY_INCLUDE_DIRS}
        ${MEGAPIXELS_INCLUDE_DIRS}
        ${XLIB_INCLUDE_DIRS}
        ${XRANDR_INCLUDE_DIRS}
        ${WAYLAND_INCLUDE_DIRS}
        ${JPEG_INCLUDE_DIRS}
)
LINK_DIRECTORIES(
        ${GTK4_LIBRARY_DIRS}
        ${FEEDBACK_LIBRARY_DIRS}
        ${DNG_LIBRARY_DIRS}
        ${ZBAR_LIBRARY_DIRS}
        ${EPOXY_LIBRARY_DIRS}
        ${MEGAPIXELS_LIBRARY_DIRS}
        ${XLIB_LIBRARY_DIRS}
        ${XRANDR_LIBRARY_DIRS}
        ${WAYLAND_LIBRARY_DIRS}
        ${JPEG_DIRS}
)

# Add other flags to the compiler
ADD_DEFINITIONS(
        ${GTK4_CFLAGS_OTHER}
        ${FEEDBACK_CFLAGS_OTHER}
        ${DNG_CFLAGS_OTHER}
        ${ZBAR_CFLAGS_OTHER}
        ${EPOXY_CFLAGS_OTHER}
        ${MEGAPIXELS_CFLAGS_OTHER}
        ${XLIB_CFLAGS_OTHER}
        ${XRANDR_CFLAGS_OTHER}
        ${WAYLAND_CFLAGS_OTHER}
        ${JPEG_OTHER}
)

find_program(GLIB_COMPILE_RESOURCES NAMES glib-compile-resources REQUIRED)
set(GRESOURCE_C   megapixels.gresource.c)
set(GRESOURCE_XML data/me.gapixels.Megapixels.gresource.xml)

add_custom_command(
        OUTPUT ${CMAKE_CURRENT_BINARY_DIR}/${GRESOURCE_C}
        WORKING_DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR}/data
        COMMAND ${GLIB_COMPILE_RESOURCES}
        ARGS
        --generate-source
        --target=${CMAKE_CURRENT_BINARY_DIR}/${GRESOURCE_C}
        ../${GRESOURCE_XML}
        VERBATIM
        MAIN_DEPENDENCY ${GRESOURCE_XML}
)

add_custom_target(
        dummy-resource
        DEPENDS ${CMAKE_CURRENT_BINARY_DIR}/${GRESOURCE_C}
)

file(GLOB srcs src/*.c src/*h)
add_executable(megapixels-gtk ${srcs} medianame.h ${CMAKE_CURRENT_BINARY_DIR}/${GRESOURCE_C} src/state.h)
set_source_files_properties(
        ${CMAKE_CURRENT_BINARY_DIR}/${GRESOURCE_C}
        PROPERTIES GENERATED TRUE
)
add_dependencies(megapixels-gtk dummy-resource)
target_link_libraries(megapixels-gtk
        m
        ${GTK4_LIBRARIES}
        ${FEEDBACK_LIBRARIES}
        ${DNG_LIBRARIES}
        ${ZBAR_LIBRARIES}
        ${EPOXY_LIBRARIES}
        ${MEGAPIXELS_LIBRARIES}
        ${XLIB_LIBRARIES}
        ${XRANDR_LIBRARIES}
        ${WAYLAND_LIBRARIES}
        ${JPEG_LIBRARIES}
        )

add_compile_definitions(VERSION="${PROJECT_VERSION}")
add_compile_definitions(SYSCONFDIR="/etc")
add_compile_definitions(DATADIR="/usr/share")